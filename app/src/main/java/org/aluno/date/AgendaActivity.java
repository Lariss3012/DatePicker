package org.aluno.date;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created on 06/11/2017.
 */

public class AgendaActivity extends AppCompatActivity {
    Calendar diaSel,diaAtual;
    TextView tvParidade, tvDiasFaltam, tvNiver;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);
        tvParidade  = (TextView) findViewById(R.id.tvParidade);
        tvDiasFaltam  = (TextView) findViewById(R.id.tvDiasFaltam);
        tvNiver  = (TextView) findViewById(R.id.tvNiver);
        int day = getIntent().getExtras().getInt("Dia");
        int month = getIntent().getExtras().getInt("Mes");
        int year = getIntent().getExtras().getInt("Ano");
        diaAtual = Calendar.getInstance();
        diaSel = Calendar.getInstance();
        diaSel.set(year, month, day);

        preencher();
    }

    private void preencher(){
        if((diaSel.get(Calendar.DAY_OF_MONTH)%2)==0){
            tvParidade.setText("O dia é par");
        }else{
            tvParidade.setText("O dia é impar");
        }
        long difdia;
        difdia =  diaSel.getTimeInMillis() - diaAtual.getTimeInMillis();
        difdia = TimeUnit.MILLISECONDS.toDays(difdia);
        String s = String.valueOf(Math.abs(difdia));
        if(difdia > 0){
            tvDiasFaltam.setText("Faltam "+s+" dias");
        }
        else{
            if(difdia < 0){
                tvDiasFaltam.setText("Foi a "+s+" dias");
            }
            else{
                tvDiasFaltam.setText("É hoje!");
            }
        }
        if( ((diaSel.get(Calendar.DAY_OF_MONTH)/1 == 13) && (diaSel.get(Calendar.MONTH)/1 == 8)) ||
        ((diaSel.get(Calendar.DAY_OF_MONTH)/1 == 30) && (diaSel.get(Calendar.MONTH)/1 == 3)) ){
            tvNiver.setText("Aniversario de um dos programadores experientes responsaveis por esse aplicativo");
        }




    }


}
