package org.aluno.date;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    DatePicker dtP;
    Button bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dtP = (DatePicker) findViewById(R.id.dtP);
        bt = (Button) findViewById(R.id.bt);
    }

    public void agenda(View v){

        int day = dtP.getDayOfMonth();
        int month = dtP.getMonth();
        int year =  dtP.getYear();

        /*
        Milisegundos
        dtP.setMaxDate();
        dtP.setMinDate();
        dtP.getMaxDate();
        dtP.getMinDate();

        dtP.setFirstDayOfWeek(int);

        dtP.updateDate(int(ano),int(mes),int(dia))
        */

        Context contexto = getApplicationContext();
        Intent objIntent = new Intent(contexto, AgendaActivity.class);
        objIntent.putExtra("Dia", day);
        objIntent.putExtra("Mes", month);
        objIntent.putExtra("Ano", year);

        startActivity(objIntent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
